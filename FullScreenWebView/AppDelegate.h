//
//  AppDelegate.h
//  FullScreenWebView
//
//  Created by MJ의 iMac on 2016. 3. 2..
//  Copyright © 2016년 uxlayer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

