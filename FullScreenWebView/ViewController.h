//
//  ViewController.h
//  FullScreenWebView
//
//  Created by MJ의 iMac on 2016. 3. 2..
//  Copyright © 2016년 uxlayer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic, strong) IBOutlet UIView *splashImageView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, strong) IBOutlet UIWebView *webView;

@end

