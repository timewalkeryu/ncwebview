//
//  ViewController.m
//  FullScreenWebView
//
//  Created by MJ의 iMac on 2016. 3. 2..
//  Copyright © 2016년 uxlayer. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _webView.delegate = self;
    
    [_indicator startAnimating];

    int cacheSizeMemory = 16 * 1024 * 1024; // 16MB
    int cacheSizeDisk = 128 * 1024 * 1024; // 1280MB
    NSURLCache* sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"];
    [NSURLCache setSharedURLCache:sharedCache];
    
    NSString* url = @"http://115.68.22.198:9900/20160529/s1/s1.html";
    
    NSURL* nsUrl = [NSURL URLWithString:url];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:120];
    
    [_webView loadRequest:request];
    // [self loadWebView];
    _webView.scrollView.bounces = NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [_indicator stopAnimating];
    _indicator.hidden = YES;
    _splashImageView.hidden = YES;
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error {
    [_indicator stopAnimating];
    _indicator.hidden = YES;
    _splashImageView.hidden = YES;
}

@end
