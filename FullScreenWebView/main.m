//
//  main.m
//  FullScreenWebView
//
//  Created by MJ의 iMac on 2016. 3. 2..
//  Copyright © 2016년 uxlayer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
